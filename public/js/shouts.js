(function($){
	$('.deleteShout').click(function(e){
		e.preventDefault();
		var shoutId = $(e.currentTarget).attr('id');
		$.ajax({
			url : '/deleteShout/'+shoutId
		})
		.done(function(){
			$(e.currentTarget).parent().parent().parent().html('<h2>Shout deleted</h2>').fadeOut();
		})
		.fail(function(){
			$('#alert').text('Deleting shout failed,try again later').delay(2000).fadeOut(1500);
		})
	})

})(jQuery);