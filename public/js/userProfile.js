(function($){
	$('#updateKarmaSubmit').click(function(e){
		$('#updateKarmaSubmit').addClass('disabled');
		e.preventDefault();
		var score = parseInt($('#updateKarmaInput').val());
		if(score < 100){
			$.ajax({
				url 	: '/updateKarma',
				data 	: {
					score 	: score,
					userId 	: window.location.href.split('/').reverse()[0]
				} 
			})
			.done(function(){
				window.location.reload();
				$('#updateKarmaSubmit').text('Updated').removeClass('disabled').text('Update');
				$('#updateKarmaInput').val('');
			})
			.fail(function(){
				$('#updateKarmaInput').val('');
				$('#updateKarmaSubmit').text('Failed').removeClass('disabled').text('Update');
			})
		}	
		else return;
	})

	$('#deleteAccount').click(function(e){
		e.preventDefault();
		var userId = window.location.href.split('/').reverse()[0];
		var sure   = window.confirm('Are you sure you want to delete this profile ? ');
		var fbId   = $('a').eq(0).attr('href').split('/').reverse()[0];
		if(!sure) return;
		$.ajax({
			url 	: '/deleteUser/'+userId,
			data 	: {
				fbId : fbId
			}
		})
		.done(function(){
			window.location = '/';
		})
		.fail(function(){
			alert('Failed');
		})
	});

})(jQuery);