var sendToSocket 	= require('../socket');
var async 			= require('async');
var _ 	   			= require('lodash'),
	fs 		  		= require('fs'),
	models 			= require('./models/models'),
	mongoose 		= require('./models/dbConnection/dbConnection')
	request   		= require('request');


module.exports.default = function(req,res){
	if(!req.session.secret) res.render('login');
	else {
		models.OnlineUsers.count({},function(err,onlineUsersCount){
			res.render('home',{
				onlineUsersCount : onlineUsersCount
			})
		})
	}
}

module.exports.home = function(req,res){
	var id 		 = req.body.id,
		password = req.body.password;
	if(password !== 'agent47barneystinsonmikeross') res.send(400);
	else{
			models.OnlineUsers.count({},function(err,count){
				if(err) console.log(err);
				else {
					req.session.secret = password;
					res.render('home',{
						onlineUsersCount : count
					});
				}
			})
	}
}

module.exports.shouts = function(req,res){
	if(_.isUndefined(req.query.pageNo) || req.query.pageNo === '0'){
		models.Shouts.find({}).sort({created_At : -1}).limit(10).exec(function(err,docs){
			if(err) console.log(err);
			else {
				var shouts = _.map(docs,function(doc){
					return {
						content 	: doc.content,
						tags 		: doc.tags,
						reportCount : doc.reportLog.length,
						shouter_id 	: doc.shouter_id,
						_id 		: doc._id
					}
				})
				
				res.render('shouts',{
					shouts : shouts,
					nextPageNo : 1,
					prevPageNo : 0
				});
			}
		})
	}
	else {
		pageNo = parseInt(req.query.pageNo);
		models.Shouts.find({}).sort({created_At : -1}).skip(pageNo*10).limit(10).exec(function(err,docs){
			if(err) console.log(err);
			else {
				var shouts = _.map(docs,function(doc){
					return {
						content 	: doc.content,
						tags 		: doc.tags,
						reportCount : doc.reportLog.length,
						shouter_id 	: doc.shouter_id
					}
				})
				
				res.render('shouts',{
					shouts : shouts,
					nextPageNo : pageNo+1,
					prevPageNo : pageNo-1
				});
			}
		})
	}
}

module.exports.onlineUsers = function(req,res){
	models.OnlineUsers.find({},'userPic user_id rank fbId name').sort({rank : 1}).exec(function(err,users){
		var onlineUsersCount = users.length;
		res.render('onlineUsers',{
			users : users,
			onlineUsersCount : onlineUsersCount
		}); 
	})
}

module.exports.userProfile = function(req,res){
	var userId = req.params.id;
	models.Shouts.count({shouter_id : userId},function(err,count){
		models.UserDetail.findOne({_id : userId},function(err,doc){
			res.render('userProfile',{
				name 		 	: doc.name,
				fbId 			: doc.fbId,				gender  	 	: doc.gender === true ? 'Male' : 'Female',
				fbLikesCount 	: doc.fbLikes.length,
				fbFriendsCount 	: doc.fbFriends.length,
				contactsCount 	: doc.contacts.length,
				shoutsCount 	: count,
				karma 			: doc.karma,
				normalPic 		: doc.normalPic,
				reportCount 	: doc.reportCount 
			})
		})
	})
}

module.exports.deleteShout = function(req,res){
	//delete shout
	//delete any thread created with this shout, delete that thread from both user's allThreads
	//notify the creater and also reduce the points
	var shoutId = req.params.shoutId;
	models.Shouts.findOneAndRemove({_id : shoutId},function(err,doc){
		_.each(doc.threads,function(thread){
			models.Threads.remove({_id : thread.threadId},function(err){
				if(err) console.log(err);
			})
			models.UserDetail.update({_id : doc.shouter_id,'allThreads.threadId' : thread.threadId},{$pull : {allThreads : 'allThreads.$'}},function(err){
				if(err) console.log(err);
			})
			models.UserDetail.update({_id : thread.responderId,'allThreads.threadId' : thread.threadId},{$pull : {allThreads : 'allThreads.$'}},function(err){
				if(err) console.log(err);
			})
		});

		models.UserDetail.sendNotification(5,{userId : doc.shouter_id},function(isOk){
			if(!isOk) console.log('Something wrong sending notification');
		})

		models.UserDetail.update({_id : doc.shouter_id},{$inc : {karma : -10}},function(err){
			if(err) console.log(err);
			res.send(200);
		})
	})

}

module.exports.updateKarma = function(req,res){
	var score 	= req.query.score,
		userId 	= req.query.userId;

	models.UserDetail.updateScore(0,{userId : userId,score : score});
	res.send(200);
}

module.exports.deleteUser = function(req,res){
	//delete userdetail,profile,status,onlineusers,threads with partiicipants,chats with participants,shouts with shouterId,
	//remove from responders of all shouts partiicpated
	//remove FB Archive

	//An instance is left in PreRegister
	var userId = mongoose.Types.ObjectId(req.params.userId),
		fbId   = req.query.fbId;

	models.UserDetail.remove({_id : userId},function(err){
		if(err) console.log(err);
	})

	models.UserProfile.remove({user_id : userId},function(err){
		if(err) console.log(err);
	})

	models.UserStatus.remove({user_id : userId},function(err){
		if(err) console.log(err);
	})

	models.FBArchive.remove({user_id : userId},function(err){
		if(err) console.log(err);
	})

	models.Chats.remove({$or : [{userId : userId},{twinId : userId}]},function(err){
		if(err) console.log(err);
	})

	models.Shouts.remove({shouter_id : userId},function(err){
		if(err) console.log(err);
	})

	models.Threads.remove({participants : userId},function(err){
		if(err) console.log(err);
	})

	models.Session.findOneAndRemove({'data.userid' : userId},function(err,doc){
		if(err) console.log(err);
		console.log(doc);
	})

	models.Shouts.update({responders : userId},{$pull : {responders : userId}},{multi : true},function(err){
		if(err) console.log(err)
	})

	models.Misc.findOneAndUpdate({},{$push : {bannedUsers : fbId}},function(err){
		if(err) console.log(err);
		res.send(200);
	})

}

module.exports.userSearch = function(req,res){
	res.render('userSearch')
}

module.exports.searchUser = function(req,res){
	var userName = req.query.name;
	models.UserProfile.textSearch(userName,function(err,docs){
		if(err || !docs.results.length){
			res.json([{}]);
		}
		else {
			var data = _.pluck(docs.results,'obj');
			var response = [];
				_.each(data,function(obj){
					response.push(_.pick(obj,'picUrl','name','user_id'));
				})
			res.json(response);
		}
	})
}

module.exports.stats = function(req,resp){
	async.parallel([
		function(cbt){
			models.UserDetail.count({},cbt);
		},
		function(cbt){
			models.Chats.count({},cbt);
		},
		function(cbt){
			models.Threads.count({},cbt);
		},
		function(cbt){
			models.Shouts.count({},cbt);
		}
	],function(err,res){
		resp.render('stats',{
			totalUsers 	 : res[0],
			totalChats 	 : res[1],
			totalThreads : res[2],
			totalShouts  : res[3]
		})
	})
}

module.exports.pidQuestions = function(req,res){
	var pid = req.params.pid;
	models.Questions.find({pid : pid},function(err,docs){
		res.render('Questions',{questions : docs});
	})
}

module.exports.pidPssts = function(req,res){
	var pid = req.params.pid;
	models.PSST.find({pid : pid},function(err,docs){
		res.render('Psst',{pssts : docs})
	})
}

module.exports.addQuestion = function(req,res){
	var question = req.body.question,
		pid 	 = req.body.pid;

	models.Questions.create({
		q 	: question,
		pid : pid,
		randomNo : Math.random()
	},function(err,doc){
		if(err || !doc) res.send(500);
		else res.send(200);
	})
}

module.exports.addPSST = function(req,res){
	var psst = req.body.psst,
		pid  = req.body.pid; 

	models.PSST.create({
		psst : psst,
		pid  : pid,
		randomNo : Math.random()
	},function(err,doc){
		if(err || !doc) res.send(500);
		else res.send(200);
	})
}

