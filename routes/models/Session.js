var mongoose =  require('./dbConnection/dbConnection');

var sessionSchema = new mongoose.Schema({
    sid : {
        type : String,
        required : true,
        unique : true,
        index  : true
    },
    data : {},
    lastAccess : {
        type : Date,
        index : true
    },
    expires : {
        type : Date,
        index : true
    }
});

module.exports = mongoose.model('session',sessionSchema);