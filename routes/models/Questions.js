var mongoose   	= require('./dbConnection/dbConnection');
var Schema 	 = mongoose.Schema;
var ObjectId = Schema.Types.ObjectId; 

var questionSchema = new Schema({
		q 		   : String,
		pid 	   : {type : Number,index : true},
		randomNo   : {type : Number,index : true}
})

module.exports = mongoose.model('Question',questionSchema);