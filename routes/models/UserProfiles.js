var mongoose   	= require('./dbConnection/dbConnection');
var Schema 	 = mongoose.Schema;
var ObjectId = Schema.Types.ObjectId; 

var notificationSchema = require('./schemas/notificationSchema.js');

var userProfileSchema 	= new Schema({
	user_id			: {type : ObjectId, index : true},
	name 			: {type: String, index : true },
	picUrl 			: {type: String,default : '/img/ppics/default.png'},
	rank 			: {type : Number,default: 3,min : 1,max : 5},
	contacts_c 		: {type: Number, min: 0,default:0},
	newThreads 		: [{threadId : {type : ObjectId} ,sender: String,content : String ,timeSent : Date,senderPic : String,_id : false}],
	newNotifications: [notificationSchema]
});

var textSearch = require('mongoose-text-search'); 
userProfileSchema.plugin(textSearch);
userProfileSchema.index({name : 'text'});

module.exports = mongoose.model('UserProfile',userProfileSchema);


