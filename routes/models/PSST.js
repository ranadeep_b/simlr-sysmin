var mongoose   	= require('./dbConnection/dbConnection');
var Schema 	 = mongoose.Schema;
var ObjectId = Schema.Types.ObjectId; 

var psstSchema = new Schema({
		psst 	   : String,
		pid 	   : {type : Number,index : true},
		randomNo   : {type : Number,index : true}
})

module.exports = mongoose.model('PSST',psstSchema);