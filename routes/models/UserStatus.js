var mongoose   	= require('./dbConnection/dbConnection');
var Schema 	 = mongoose.Schema;
var ObjectId = Schema.Types.ObjectId; 

var userStatusSchema 	= new Schema({
	user_id		: {type: ObjectId, index: true},
	status 		: {type: Boolean, index:true, default:true },
	lastOnline 	: {type: Number, default: Date.now()}
})

module.exports = mongoose.model('UserStatus',userStatusSchema);

