var express 	= require('express');
var http 		= require('http');
var mongoose  	= require('./routes/models/dbConnection/dbConnection');
var sendToSocket = require('./socket');


var app = express();
var server = http.createServer(app);
server.listen(1309);
console.log('ICA on port 1309');

app.configure(function(){
	app.set('view engine','jade');
	app.set('views','./views')
	app.use(express.logger('dev'));
	app.use(express.static(__dirname+'/public'));
	app.use(express.errorHandler({
		dumpExceptions: true,
		showStack	  : true
	}));
	app.use(express.bodyParser({keepExtensions : true}));
	app.use(express.cookieParser("shoutbox"));
	app.use(express.session({
		secret : 'shoutbox',
		cookie : {
			maxAge : null // 60,000k
		}
	}));
	app.use(app.router);
});


var routes = require('./routes');

var authenticate = function(req,res,next){
	if(typeof req.session.secret !== 'undefined' && req.session.secret === 'agent47barneystinsonmikeross') next();
	else res.redirect('/');
}

app.get('/',routes.default);

app.post('/home',routes.home);

app.get('/shouts',authenticate,routes.shouts);

app.get('/onlineUsers',authenticate,routes.onlineUsers);

app.get('/user/:id',authenticate,routes.userProfile);

app.get('/deleteShout/:shoutId',routes.deleteShout);

app.get('/updateKarma',routes.updateKarma);

app.get('/deleteUser/:userId',routes.deleteUser);

app.get('/userSearch',routes.userSearch);

app.get('/searchUser',routes.searchUser);

app.get('/stats',routes.stats);

app.get('/questions/:pid',routes.pidQuestions);

app.get('/psst/:pid',routes.pidPssts);

app.post('/addQuestion',routes.addQuestion);

app.post('/addPsst',routes.addPSST);


